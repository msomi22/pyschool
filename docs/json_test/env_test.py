import os

'''
export POSTGRES_URL="127.0.0.1:5432"
export POSTGRES_USER="school"
export POSTGRES_PW="AllaManO1"
export POSTGRES_DB="schooldb"

nano ~/.bashrc
source ~/.bashrc

'''

def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

# the values of those depend on your setup
POSTGRES_URL = get_env_variable("POSTGRES_URL")
POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PW = get_env_variable("POSTGRES_PW")
POSTGRES_DB = get_env_variable("POSTGRES_DB")

print 'POSTGRES_URL', POSTGRES_URL
print 'POSTGRES_USER', POSTGRES_USER
print 'POSTGRES_PW', POSTGRES_PW
print 'POSTGRES_DB', POSTGRES_DB



'''
alias mgit='cd /media/peter/1c856d9d-d881-4510-981c-cf6c1a40a4d0/peter/git' 
alias jboss='cd /opt/Programs/WildFly/8.2.0/bin' 
alias cat9='cd /opt/Programs/tomcat/9.0.6/bin'
alias cat7='cd /opt/Programs/tomcat/7.0.79/bin'

'''