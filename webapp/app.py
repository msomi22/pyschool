from flask import Flask,render_template, request, flash ,redirect, url_for, session
from models.Model import Person 
from flask_bootstrap import Bootstrap  
from flask import send_file 
from flask_sqlalchemy import SQLAlchemy 
import os
import datetime
from forms import AcountForm 

'''
'''
def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

POSTGRES_URL = get_env_variable("POSTGRES_URL")
POSTGRES_USER = get_env_variable("POSTGRES_USER")
POSTGRES_PW = get_env_variable("POSTGRES_PW")
POSTGRES_DB = get_env_variable("POSTGRES_DB")         


DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER,pw=POSTGRES_PW,url=POSTGRES_URL,db=POSTGRES_DB) 

app = Flask(__name__)
app.secret_key = 'peter_mwenda_njeru_1234567890' 
bootstrap = Bootstrap(app)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # silence the deprecation warning

db = SQLAlchemy(app) 

'''
Account Model
'''
class Account(db.Model):

	def __init__(self,uuid,isactive,name,motto,website,logo,signature,username,password,\
		mobile,email,address,town,isboarding,ismixed,lastupdated,creationdate):
		self.uuid = uuid 
		self.isactive = isactive
		self.name = name
		self.motto = motto
		self.website = website
		self.logo = logo
		self.signature = signature
		self.username = username
		self.password = password
		self.mobile = mobile
		self.email = email
		self.address = address
		self.town = town
		self.isboarding = isboarding
		self.ismixed = ismixed
		self.lastupdated = lastupdated
		self.creationdate = creationdate

	__tablename__ = 'account'
	id = db.Column(db.Integer, primary_key=True)
	uuid = db.Column(db.String(100), unique=True, nullable=True)
	isactive = db.Column(db.String(2), unique=False, nullable=True)
	name = db.Column(db.String(255), unique=True, nullable=True)
	motto = db.Column(db.String(255), unique=False, nullable=True)
	website = db.Column(db.String(255), unique=True, nullable=True)
	logo = db.Column(db.String(100), unique=False, nullable=True)
	signature = db.Column(db.String(255), unique=False, nullable=True)
	username = db.Column(db.String(255), unique=True, nullable=True)
	password = db.Column(db.String(255), unique=False, nullable=True)
	mobile = db.Column(db.String(15), unique=True, nullable=True)
	email = db.Column(db.String(100), unique=True, nullable=True)
	address = db.Column(db.String(100), unique=False, nullable=True)
	town = db.Column(db.String(100), unique=False, nullable=True)
	isboarding = db.Column(db.String(2), unique=False, nullable=True)
	ismixed = db.Column(db.String(2), unique=False, nullable=True)
	lastupdated = db.Column(db.String(255), unique=False, nullable=True)
	creationdate = db.Column(db.DateTime(255), unique=False, nullable=True)
	
	def __repr__(self): 
		return '<Account %r>' % self.uuid  


'''
    **************************************************************************************
    Account model - end
'''



@app.route('/')
def index():
	return render_template('index.html') 

@app.route('/return_file/') 
def return_file():
	return send_file('/home/peter/git/pyschool/webapp/static/school.war', attachment_filename='school.war') 

@app.route('/file_downloads') 
def file_downloads():
	return render_template('download.html') 


@app.route('/user/<name>')
def User(name):
	person = Person("Peter Mwenda!") 
	#'Hello ' +str(person) 
	return render_template('user.html', name=name) 	 

@app.route('/account') 
def account():
	return render_template('account.html', accounts = Account.query.all())   

@app.route('/profile/<account_id>')  
def load_acc_profile(account_id): 
	account = Account.query.filter_by(uuid=account_id).first()  
	return render_template('account_p.html', account=account) 

@app.route('/acc_update', methods=['GET', 'POST']) 
def updateAccount():	
	 form = AcountForm(request.form) 
	 if request.method == 'POST' and form.validate():  
	 	account = Account.query.filter_by(uuid=str(form.uuid.data)).first() 
	 	account.isactive = form.isactive.data
	 	account.name =  form.name.data
	 	account.motto =  form.motto.data
	 	account.website =  form.website.data
	 	account.logo =  form.logo.data
	 	account.signature =  form.signature.data
	 	account.username =  form.username.data
	 	account.mobile =  form.mobile.data
	 	account.email =  form.email.data
	 	account.address =  form.address.data
	 	account.town =  form.town.data
	 	account.isboarding = form.isboarding.data
	 	account.ismixed = form.ismixed.data
	 	account.lastupdated = str(datetime.datetime.now()) 
	 	db.session.commit()

	 	flash('Account updated successfully') 
	 	return redirect(url_for('load_acc_profile',account_id=form.uuid.data))
	 return redirect(url_for('load_acc_profile',account_id=form.uuid.data))
	 


if __name__ == '__main__': 
	app.run(debug=True)
	#app.run("0.0.0.0", "5010") 
