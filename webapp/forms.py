from wtforms import Form, StringField, validators, SubmitField

class AcountForm(Form):  
    uuid = StringField('uuid', [validators.DataRequired()])
    isactive = StringField('isactive', [validators.DataRequired()])
    name = StringField('name', [validators.DataRequired()])
    motto = StringField('motto', [validators.DataRequired()])
    website = StringField('website', [validators.DataRequired()])
    logo = StringField('logo', [validators.DataRequired()])
    signature = StringField('signature', [validators.DataRequired()])
    username = StringField('username', [validators.DataRequired()])
    password = StringField('password', [validators.DataRequired()])
    mobile = StringField('mobile', [validators.DataRequired()])
    email = StringField('email', [validators.DataRequired()])
    address = StringField('address', [validators.DataRequired()])
    town = StringField('town', [validators.DataRequired()])
    isboarding = StringField('isboarding', [validators.DataRequired()])
    ismixed = StringField('ismixed', [validators.DataRequired()])
    

    submit = SubmitField('Update') 


